const fs = require('fs')

module.exports = {
  devServer: {
    open: process.platform === 'darwin',
    https: {
      key: fs.readFileSync('./certs/studybuddy.key'),
      cert: fs.readFileSync('./certs/studybuddy.crt')
    },
    disableHostCheck: true,
    public: 'https://studybuddy.test'
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  },

  chainWebpack: config => {
    config.module
      .rule('svg-sprite')
      .use('svgo-loader')
      .loader('svgo-loader')
  }
}
