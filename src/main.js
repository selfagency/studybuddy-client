// package dependencies
import * as Colyseus from 'colyseus.js'
import { observer } from 'vue-mutation-observer'
import consola from 'consola'
import md5 from 'blueimp-md5'
import Vue from 'vue'
import VueClipboard from 'vue-clipboard2'
import VTooltip from 'v-tooltip'

// local modules
import './registerServiceWorker'
import i18n from './i18n'
import store from './store/store'
import router from './router'
import App from './App.vue'
import SvgIcon from './components/SvgIcon.vue'

// global utils
window.Console = consola
window.debug = process.env.NODE_ENV !== 'production'

// libraries
Vue.prototype.$bus = new Vue()
Vue.prototype.$client = new Colyseus.Client('ws://localhost:2567')
Vue.prototype.$hash = md5

// vue plugins
Vue.use(observer)
Vue.use(VueClipboard)
Vue.use(VTooltip, {
  defaultPlacement: 'bottom',
  defaultDelay: 500,
  defaultTrigger: 'hover',
  disposeTimeout: 0
})

// global components
Vue.component('SvgIcon', SvgIcon)

// config settings
Vue.config.productionTip = false

// instantiate vue
new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
