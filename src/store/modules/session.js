const namespaced = true

const state = {
  creator: false,
  frameWidth: null,
  id: 123456789,
  page: '/sefaria',
  panelWidth: null
}

const mutations = {
  NAVIGATE(state, url) {
    state.page = url
  },
  SET_WIDTH(state, element) {
    state[`${element.name}Width`] = element.width
  }
}

const actions = {
  navigate({ commit }, url) {
    commit('NAVIGATE', url)
  },
  setWidth({ commit }, element) {
    commit('SET_WIDTH', element)
  },
  getWidth({ commit, rootState }) {
    if (rootState.panel.active) {
      const checkForPanel = setInterval(() => {
        const panel = document.getElementById('panel')
        if (panel) {
          const panelWidth = document.getElementById('panel').clientWidth
          const frameWidth = window.innerWidth - panelWidth
          commit('SET_WIDTH', { name: 'panel', width: panelWidth })
          commit('SET_WIDTH', { name: 'frame', width: frameWidth })
          clearInterval(checkForPanel)
        }
      }, 100)
    } else {
      commit('SET_WIDTH', { name: 'panel', width: 0 })
      commit('SET_WIDTH', { name: 'frame', width: window.innerWidth })
    }
  }
}

const getters = {
  page(state) {
    return state.page
  },
  sessionId(state) {
    return state.id
  },
  shareUrl(state) {
    return `https://studybuddy.test/${state.id}`
  }
}

export { namespaced, state, mutations, actions, getters }
