const namespaced = true

const state = {
  active: false,
  color: { name: 'Black', color: [45, 52, 54] },
  colors: [
    { name: 'Black', color: [45, 52, 54], active: true },
    { name: 'Red', color: [214, 48, 49], active: false },
    { name: 'Yellow', color: [253, 203, 110], active: false },
    { name: 'Blue', color: [9, 132, 227], active: false },
    { name: 'Green', color: [0, 184, 148], active: false }
  ],
  weight: { name: 'Small', size: 4 },
  weights: [
    { name: 'Small', size: 4, active: true },
    { name: 'Medium', size: 8, active: false },
    { name: 'Large', size: 12, active: false }
  ],
  transparent: false,
  drawing: []
}

const mutations = {
  ACTIVATE(state) {
    state.active = true
  },
  DEACTIVATE(state) {
    state.active = false
  },
  COLOR(state, color) {
    state.color = color
    state.colors = state.colors.map(c => {
      c.active = c.name === color.name
      return c
    })
  },
  WEIGHT(state, weight) {
    state.weight = weight
    state.weights = state.weights.map(w => {
      w.active = w.name === weight.name
      return w
    })
  },
  TRANSPARENT(state) {
    state.transparent = !state.transparent
  },
  DRAW(state, data) {
    state.drawing.push(data)
  },
  CLEAR(state) {
    state.drawing = []
  }
}

const actions = {
  toggle({ commit, state }) {
    state.active ? commit('DEACTIVATE') : commit('ACTIVATE')
  },
  color({ commit }, color) {
    commit('COLOR', color)
  },
  weight({ commit }, weight) {
    commit('WEIGHT', weight)
  },
  transparent({ commit }) {
    commit('TRANSPARENT')
  },
  draw({ commit }, data) {
    commit('DRAW', data)
  },
  clear({ commit }) {
    commit('CLEAR')
  }
}

const getters = {
  color(state) {
    return state.color
  },
  weight(state) {
    return state.weight
  }
}

export { namespaced, state, mutations, actions, getters }
