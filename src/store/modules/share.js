const namespaced = true

const state = {
  active: true,
  notice: null
}

const mutations = {
  ACTIVATE(state) {
    state.active = true
  },
  DEACTIVATE(state) {
    state.active = false
  },
  NOTIFY(state, notice) {
    state.notice = notice
  }
}

const actions = {
  toggle({ state, commit }) {
    state.active ? commit('DEACTIVATE') : commit('ACTIVATE')
  },
  notify({ commit }, notice) {
    commit('NOTIFY', notice)
  },
  clearNotice({ commit }) {
    commit('NOTIFY', null)
  }
}

export { namespaced, state, mutations, actions }
