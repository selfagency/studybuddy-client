const namespaced = true

const state = {
  active: true
}

const mutations = {
  ACTIVATE(state) {
    state.active = true
  },
  DEACTIVATE(state) {
    state.active = false
  }
}

const actions = {
  toggle({ commit, dispatch, state }) {
    state.active ? commit('DEACTIVATE') : commit('ACTIVATE')
    dispatch('session/getWidth', null, { root: true })
  }
}

export { namespaced, state, mutations, actions }
