import i18n from '@/i18n'

const namespaced = true

const state = {
  active: false,
  notice: null,
  marks: []
}

const mutations = {
  ACTIVATE(state) {
    state.active = true
  },
  DEACTIVATE(state) {
    state.active = false
  },
  ADD(state, mark) {
    state.marks.push(mark)
  },
  DEL(state, mark) {
    state.marks = state.marks.filter(m => m.id !== mark)
  },
  NOTIFY(state, notice) {
    state.notice = notice
  }
}

const actions = {
  toggle({ commit, state }) {
    state.active ? commit('DEACTIVATE') : commit('ACTIVATE')
  },
  addMark({ commit, getters }, mark) {
    !getters.hasMark(mark.id) ? commit('ADD', mark) : commit('NOTIFY', i18n.t('bookmarks.exists'))
  },
  delMark({ commit }, mark) {
    commit('DEL', mark)
  },
  clearNotice({ commit }) {
    commit('NOTIFY', null)
  }
}

const getters = {
  hasMark: state => id => {
    return state.marks.find(m => m.id === id)
  }
}

export { namespaced, state, mutations, actions, getters }
