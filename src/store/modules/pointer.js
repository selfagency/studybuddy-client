const namespaced = true

const state = {
  active: false
}

const mutations = {
  ACTIVATE(state) {
    state.active = true
  },
  DEACTIVATE(state) {
    state.active = false
  }
}

const actions = {
  toggle({ state, commit }) {
    state.active ? commit('DEACTIVATE') : commit('ACTIVATE')
  }
}

export { namespaced, state, mutations, actions }
