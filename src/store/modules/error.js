const namespaced = true

const state = {
  message: null
}

export { namespaced, state }
