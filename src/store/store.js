import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import localforage from 'localforage'

import * as bookmarks from './modules/bookmarks'
import * as error from './modules/error'
import * as lock from './modules/lock'
import * as mute from './modules/mute'
import * as panel from './modules/panel'
import * as pen from './modules/pen'
import * as pointer from './modules/pointer'
import * as session from './modules/session'
import * as share from './modules/share'

const tools = ['pointer', 'pen', 'bookmarks', 'share']

Vue.use(Vuex)
const persistentStorage = new VuexPersistence({
  storage: localforage
})

export default new Vuex.Store({
  modules: {
    bookmarks,
    error,
    lock,
    mute,
    panel,
    pen,
    pointer,
    session,
    share
  },
  actions: {
    swapTool({ state, commit }, tool) {
      switch (tool) {
        case 'lock':
        case 'mute':
          !state[tool].active ? commit(`${tool}/ACTIVATE`) : commit(`${tool}/DEACTIVATE`)
          break
        default:
          tools
            .filter(t => t !== tool)
            .forEach(t => {
              commit(`${t}/DEACTIVATE`)
            })
          commit(`${tool}/ACTIVATE`)
          break
      }
    }
  },
  plugins: [persistentStorage.plugin]
})
