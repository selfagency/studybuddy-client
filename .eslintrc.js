module.exports = {
  root: true,
  env: {
    node: true,
    es6: true
  },
  plugins: ['import', 'node', 'prettier', 'promise', 'security', 'standard', 'vue', 'vue-a11y'],
  extends: ['plugin:vue/essential', '@vue/prettier', 'plugin:vue-a11y/base'],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    indent: ['off'],
    'no-eval': ['error'],
    'no-script-url': ['error'],
    'linebreak-style': ['warn', 'unix'],
    quotes: ['warn', 'single'],
    semi: ['warn', 'never'],
    'no-undef': ['off'],
    'require-jsdoc': [
      'off',
      {
        require: null,
        FunctionDeclaration: [true],
        MethodDefinition: [true],
        ClassDeclaration: [true],
        ArrowFunctionExpression: [true],
        FunctionExpression: [true]
      }
    ],
    'valid-jsdoc': ['warn'],
    'no-useless-escape': ['off'],
    'key-spacing': ['off'],
    'wrap-iife': ['error', 'inside'],
    'handle-callback-err': ['error'],
    'callback-return': ['error'],
    'no-mixed-requires': ['error'],
    'no-unused-vars': [
      'warn',
      {
        argsIgnorePattern: 'i|res|next|^err'
      }
    ],
    'security/detect-non-literal-fs-filename': ['off'],
    'security/detect-non-literal-regexp': ['off'],
    'security/detect-non-literal-require': ['off'],
    'security/detect-object-injection': ['off']
  },
  parserOptions: {
    parser: 'babel-eslint',
    allowImportExportEverywhere: true
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)'],
      env: {
        jest: true
      }
    }
  ]
}
